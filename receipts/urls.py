from django.urls import path
from receipts.views import (
    receipt_view,
    receipt_create,
    receipt_categories,
    account_list,
    create_category,
    account_view
)


urlpatterns = [
    path("", receipt_view, name="home"),
    path("create/", receipt_create, name="create_receipt"),
    path("categories/", receipt_categories, name="category_list"),
    path("accounts/", account_list, name="account_list"),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/create/", account_view, name="create_account"),
]
